import pygame
import sys
import random
import os
import pygame.mixer
from pygame.locals import *

# Load image
def load_image(filename):
    internal_path = os.path.join("_internal", filename)
    src_path = os.path.join("src", filename)
    if os.path.exists(internal_path):
        return pygame.image.load(internal_path)
    elif os.path.exists(src_path):
        return pygame.image.load(src_path)

# Load sounds
def load_sfx(filename):
    internal_path = os.path.join("_internal", filename)
    src_path = os.path.join("src", filename)
    if os.path.exists(internal_path):
        return pygame.mixer.Sound(internal_path)
    elif os.path.exists(src_path):
        return pygame.mixer.Sound(src_path)

# Initialize pygame
pygame.init()

# Set up the window
WIDTH, HEIGHT = 300, 350
WIN = pygame.display.set_mode((WIDTH, HEIGHT))

# Game variables
board = [['', '', ''],
         ['', '', ''],
         ['', '', '']]
current_player = 'X'

# Icon
icon_size = (32, 32)
icon_path = load_image("icons/icon.png")
icon_image = pygame.transform.scale(icon_path, icon_size)
pygame.display.set_icon(icon_image)

# Load custom font
if os.path.exists(os.path.join("_internal", "fonts", "RETROTECH.ttf")):
    font_path = os.path.join("_internal", "fonts", "RETROTECH.ttf")
elif os.path.exists(os.path.join("fonts", "RETROTECH.ttf")):
    font_path = os.path.join("fonts", "RETROTECH.ttf")

# Variables
background_image = load_image("generics/background.png")
background_start_image = load_image("generics/background_singleplayer.png")
background_game_over_image = load_image("generics/background_game_over.png")
sfx_click = load_sfx("sounds/sfx_click.wav")
sfx_win = load_sfx("sounds/sfx_win.wav")
sfx_lose = load_sfx("sounds/sfx_lose.wav")
sfx_draw = load_sfx("sounds/sfx_draw.wav")

# Function to draw the table image
def draw_table(score):
    WIN.blit(background_start_image, (0, 0))
    table_image = load_image("generics/table.png")
    WIN.blit(table_image, (0, 0))
    font = pygame.font.Font(font_path, 30)
    score_text_x = font.render(f"X: {score['X']}", True, (0, 0, 0))
    score_text_o = font.render(f"O: {score['O']}", True, (0, 0, 0))
    score_text_tie = font.render(f"Tie: {score['Tie']}", True, (0, 0, 0))
    total_width = sum([font.size(text)[0] for text in ["X: 0", "O: 0", "Tie: 0"]])
    text_height = font.size("X: 0")[1]
    start_x = (WIDTH - total_width) // 2
    x_x = start_x - 38
    x_o = start_x + font.size(f"X: {score['X']}")[0] + 115
    x_tie = start_x + total_width - font.size(f"Tie: {score['Tie']}")[0] - 48
    WIN.blit(score_text_x, (x_x, HEIGHT - 15 - text_height // 2))
    WIN.blit(score_text_o, (x_o, HEIGHT - 15 - text_height // 2))
    WIN.blit(score_text_tie, (x_tie, HEIGHT - 15 - text_height // 2))

# Function to draw the X's and O's using images
def draw_symbols():
    font = pygame.font.Font(font_path, 70)
    for row in range(3):
        for col in range(3):
            if board[row][col] == 'X':
                text_surface = font.render('X', True, (0, 0, 0))
                WIN.blit(text_surface, (col * 100 + 30, row * 100 + 30))
            elif board[row][col] == 'O':
                text_surface = font.render('O', True, (0, 0, 0))
                WIN.blit(text_surface, (col * 100 + 30, row * 100 + 30))

# Function to handle mouse clicks
def handle_click(row, col):
    global current_player
    if 0 <= row < 3 and 0 <= col < 3:
        if board[row][col] == '' and not check_winner() and not check_draw():
            board[row][col] = current_player
            sfx_click.play()
            if current_player == 'X':
                current_player = 'O'
                if not check_winner() and not check_draw():
                    ai_move()
                current_player = 'X'

# Function to check for a winner
def check_winner():
    for row in range(3):
        if board[row][0] == board[row][1] == board[row][2] != '':
            return board[row][0]
    for col in range(3):
        if board[0][col] == board[1][col] == board[2][col] != '':
            return board[0][col]
    if board[0][0] == board[1][1] == board[2][2] != '':
        return board[0][0]
    if board[0][2] == board[1][1] == board[2][0] != '':
        return board[0][2]
    return None

# Function to check for a draw
def check_draw():
    for row in range(3):
        for col in range(3):
            if board[row][col] == '':
                return False
    return True

# Function to make the computer's move
def ai_move():
    available_moves = []
    for row in range(3):
        for col in range(3):
            if board[row][col] == '':
                available_moves.append((row, col))

    if random.random() < 0.2 and available_moves:
        random_move = random.choice(available_moves)
        board[random_move[0]][random_move[1]] = 'O'
    else:
        best_score = float('-inf')
        best_move = None
        for move in available_moves:
            row, col = move
            board[row][col] = 'O'
            score = minimax(board, False)
            board[row][col] = ''
            if score > best_score:
                best_score = score
                best_move = move
        if best_move:
            board[best_move[0]][best_move[1]] = 'O'

# Minimax algorithm
def minimax(board, is_maximizing):
    result = check_winner()
    if result:
        if result == 'X':
            return -1
        elif result == 'O':
            return 1
    elif check_draw():
        return 0

    if is_maximizing:
        best_score = float('-inf')
        for row in range(3):
            for col in range(3):
                if board[row][col] == '':
                    board[row][col] = 'O'
                    score = minimax(board, False)
                    board[row][col] = ''
                    best_score = max(score, best_score)
        return best_score
    else:
        best_score = float('inf')
        for row in range(3):
            for col in range(3):
                if board[row][col] == '':
                    board[row][col] = 'X'
                    score = minimax(board, True)
                    board[row][col] = ''
                    best_score = min(score, best_score)
        return best_score

# Reset the game
def reset_game():
    global board, current_player, score
    board = [['', '', ''],
             ['', '', ''],
             ['', '', '']]
    current_player = 'X'
    score = {'X': 0, 'O': 0, 'Tie': 0}
    main()

# Function to display the end game screen
def end_game_screen(result):
    end_game_window = pygame.display.set_mode((300, 350))
    pygame.display.set_caption("Game Over")
    end_game_window.fill((255, 255, 255))
    
    font = pygame.font.Font(font_path, 35)
    if result == 'draw':
        sfx_draw.play()
        WIN.blit(background_game_over_image, (0, 0))
        text_game_over = font.render("GAME OVER", True, (0, 0, 0))
        end_game_window.blit(text_game_over, (55, 20))
        text_draw = font.render("It's a draw!", True, (0, 0, 0))
        end_game_window.blit(text_draw, (55, 150))
    elif result == 'X':
        sfx_win.play()
        WIN.blit(background_game_over_image, (0, 0))
        text_game_over = font.render("GAME OVER", True, (0, 0, 0))
        end_game_window.blit(text_game_over, (55, 20))
        text_win = font.render("You win!", True, (0, 0, 0))
        end_game_window.blit(text_win, (75, 150))
    else:
        sfx_lose.play()
        WIN.blit(background_game_over_image, (0, 0))
        text_game_over = font.render("GAME OVER", True, (0, 0, 0))
        end_game_window.blit(text_game_over, (55, 20))
        text_lost = font.render("You lost!", True, (0, 0, 0))
        end_game_window.blit(text_lost, (75, 150))
    
    main_menu_image = load_image("generics/main_menu.png")
    play_again_image = load_image("generics/play_again.png")
    main_menu_rect = main_menu_image.get_rect(topleft=(55, 280))
    play_again_rect = play_again_image.get_rect(topleft=(55, 220))
    end_game_window.blit(main_menu_image, (55, 280))
    end_game_window.blit(play_again_image, (55, 220))

    pygame.display.update()

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == MOUSEMOTION:
                x, y = pygame.mouse.get_pos()
                if play_again_rect.collidepoint(x, y) or main_menu_rect.collidepoint(x, y):
                    pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_HAND)
                else:
                    pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_ARROW)

            elif event.type == MOUSEBUTTONDOWN:
                x, y = pygame.mouse.get_pos()
                if play_again_rect.collidepoint(x, y):
                    sfx_click.play()
                    return True, (WIDTH, HEIGHT)
                if main_menu_rect.collidepoint(x, y):
                    sfx_click.play()
                    reset_game()

# Main menu
def show_menu():
    menu_window = pygame.display.set_mode((300, 350))
    pygame.display.set_caption("Tic Tac Toe - Main Menu")
    menu_window.fill((255, 255, 255))

    font = pygame.font.Font(font_path, 35)
    text_main_menu = font.render("Tic Tac Toe", True, (0, 0, 0))
    start_image = load_image("generics/start.png")
    footer_font = pygame.font.Font(font_path, 20)
    footer_main_menu = footer_font.render("(c) Debeleac Vincenzzio Andrei", True, (0, 0, 0))
    start_image_rect = start_image.get_rect(topleft=(55, 155))

    menu_window.blit(background_image, (0, 0))
    menu_window.blit(start_image, (55, 155))
    menu_window.blit(text_main_menu, (55, 20))
    menu_window.blit(footer_main_menu, (10, 320))

    pygame.display.update()

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == MOUSEMOTION:
                x, y = pygame.mouse.get_pos()
                if start_image_rect.collidepoint(x, y):
                    pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_HAND)
                else:
                    pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_ARROW)

            elif event.type == MOUSEBUTTONDOWN:
                x, y = pygame.mouse.get_pos()
                pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_ARROW)
                if start_image_rect.collidepoint(x, y):
                    sfx_click.play()
                    return 'Singleplayer'

# Main game loop
def main():
    while True:
        mode = show_menu()
        if mode == 'Singleplayer':
            singleplayer_game()

# Singleplayer
def singleplayer_game():
    score = {'X': 0, 'O': 0, 'Tie': 0}
    pygame.display.set_caption("Tic Tac Toe")
    while True:
        run = True
        while run:
            winner = None
            while not winner and not check_draw():
                WIN.fill((255, 255, 255))
                draw_table(score)
                draw_symbols()
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        pygame.quit()
                        sys.exit()
                    elif event.type == MOUSEBUTTONDOWN:
                        mouseX, mouseY = pygame.mouse.get_pos()
                        clicked_row = mouseY // 100
                        clicked_col = mouseX // 100
                        handle_click(clicked_row, clicked_col)
                        winner = check_winner()

                pygame.display.update()

            if winner:
                if winner == 'X':
                    result = 'X'
                elif winner == 'O':
                    result = 'O'
                else:
                    result = 'Tie'
                score[result] += 1
                run, size = end_game_screen(result)
            elif check_draw():
                score['Tie'] += 1
                run, size = end_game_screen('draw')

            if run:
                pygame.display.set_mode(size)
                pygame.display.set_caption("Tic Tac Toe")
                pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_ARROW)
                for i in range(3):
                    for j in range(3):
                        board[i][j] = ''

# Main
if __name__ == "__main__":
    main()